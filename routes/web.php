<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('home', 'HomeController@index')->name('home');
    Route::post('home/upload','HomeController@upload')->name('home.upload');
    Route::get('show/{id}','HomeController@showtable')->name('home.data');
    Route::delete('home/{id}','HomeController@delete')->name('home.delete');
    Route::put('home/{id}','HomeController@update')->name('home.update');
    Route::get('data/upload','HomeController@showpublic')->name('home_public.data');
    Route::put('home/{id}/like','HomeController@like')->name('home.like');
    Route::put('home/{id}/dislike','HomeController@dislike')->name('home.dislike');
    Route::get('profile','UserController@show')->name('user.show');
    Route::get('profile/edit','UserController@edit')->name('user.edit');
    Route::put('profile/edit','UserController@update')->name('user.update');
    Route::get('/subs', function () {
        return view('subs');
    });
});
