<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\Like;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function upload(Request $request)
    {
        $file = new File;
        $file->user_id = auth()->user()->id;
        $file->created_by = auth()->user()->name;

        $upload = $request->file('file_upload');
        $nama_gambar = $upload->getClientOriginalName();
        $lokasi = public_path('upload/' . auth()->user()->id);
        //upload foto ke folder images
        $upload->move($lokasi, $nama_gambar);

        $file->url = $nama_gambar;
        $file->like = 0;
        $file->dislike = 0;
        $file->save();

        $like = new Like;
        $like->user_id = auth()->user()->id;
        $like->file_id = $file->id;
        $like->save();

        return redirect()->back();
    }

    public function like($id)
    {
        $check = Like::where('file_id',$id)->where('user_id',auth()->user()->id)->first();
        $file = File::find($id);

        if($check->already_dislike == 1)
        {
            $file->dislike -= 1;
            $file->like += 1;
            $check->already_like = 1;
            $check->already_dislike = 0;
        }
        else
        {
            $file->like += 1;
            $check->already_like = 1;
        }
        $file->update();
        $check->update();
    }

    public function dislike($id)
    {
        $check = Like::where('file_id',$id)->where('user_id',auth()->user()->id)->first();
        $file = File::find($id);
        if($check->already_like == 1)
        {
            $file->dislike += 1;
            $file->like -= 1;
            $check->already_like = 0;
            $check->already_dislike = 1;
        }
        else
        {
            $file->dislike += 1;
            $check->already_like = 1;
        }
        $file->update();
        $check->update();
    }

    public function showpublic()
    {
        $files = File::where('visibility','public')->get();

        $no = 0;
        $data = array();

        foreach($files as $file)
        {
            $no++;
            $row=array();
            $row[]=$no;
            $row[]="<a href='/upload/".auth()->user()->id."/".$file->url."' target='_blank'>$file->url</a>";
            $row[]=$file->created_at->diffForHumans();
            $row[]=$file->like;
            $row[]=$file->dislike;
            $check = Like::where('file_id',$file->id)->where('user_id',auth()->user()->id)->first();
            if($check->already_dislike == 0)
            {
                if($check->already_like == 0)
                    $row[]="<a onclick=btnlike(".$file->id.") class='btn btn-primary'><i class='fa fa-thumbs-up'></i></a><a style='margin-left:5%;' onclick=btndislike(".$file->id.") class='btn btn-danger'><i class='fa fa-thumbs-down'></i></a>";
                else
                    $row[]="<a style='margin-left:5%;' onclick=btndislike(".$file->id.") class='btn btn-danger'><i class='fa fa-thumbs-down'></i></a>";
            }
            else
            {
                if($check->already_like == 0)
                    $row[]="<a onclick=btnlike(".$file->id.") class='btn btn-primary'><i class='fa fa-thumbs-up'></i></a>";
                else
                    $row[] = "";
            }
            $data[]=$row;
        }

        $output = array("data"=>$data);
        return response()->json($output);
    }

    public function showtable($id)
    {
        $files = File::where('user_id', auth()->user()->id)->get();

        $no = 0;
        $data = array();

        foreach($files as $file)
        {
            $no++;
            $row=array();
            $row[]=$no;
            $row[]="<a href='/upload/".auth()->user()->id."/".$file->url."' target='_blank'>$file->url</a>";
            $row[]=$file->created_at->diffForHumans();
            $row[]=$file->like;
            $row[]=$file->dislike;
            if($file->visibility == 'public')
                $row[]="<a onclick='deleteData(".$file->id.")' class='btn btn-danger'><i class='fa fa-trash'></i></a><a style='margin-left:5%;' onclick='updateVisib(".$file->id.")' class='btn btn-primary'><i class='fa fa-eye'></i></a>";
            else
                $row[]="<a onclick='deleteData(".$file->id.")' class='btn btn-danger'><i class='fa fa-trash'></i></a><a style='margin-left:5%;' onclick='updateVisib(".$file->id.")' class='btn btn-primary'><i class='fa fa-eye-slash'></i></a>";
            $data[]=$row;
        }

        $output = array("data"=>$data);
        return response()->json($output);
    }

    public function delete($id)
    {
        $file = File::find($id);
        $file->delete();
    }

    public function update($id)
    {
        $file = File::find($id);
        if($file->visibility == 'private')
            $file->visibility = 'public';
        else
            $file->visibility = 'private';
        $file->update();
    }
}
