<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class UserController extends Controller
{
    public function show()
    {
        $user = User::find(auth()->user()->id);
        return view('profile')->with('user',$user);
    }

    public function edit()
    {
        $user = User::find(auth()->user()->id);
        return view('editprofile')->with('user',$user);
    }

    public function update(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $msg = '';

        if($request->has('email'))
            $user->email = $request['email'];
        if($request->has('dob'))
            $user->dob = $request['dob'];

        if($request->has('old_password') && $request->has('password') && $request->has('password_confirmation'))
        {
            if ($request['password'] != $request['password_confirmation'])
                $msg = 'fail';
            elseif (Hash::check($request['old_password'], $user->password))
                $user->password = bcrypt($request['password']);
            else
                $msg = 'error';
        }

        $user->update();

        if (!$msg)
            return redirect()->route('user.show');

        return $msg;
    }
}
