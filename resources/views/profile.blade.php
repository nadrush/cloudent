@extends('adminlte::page')

@section('title', 'Cloudent')


@section('content')
<div class="box">
    <div class="box-header with-border">
        <h1 class="box-title">Your Profile</h1>
    </div>
    <br>
    <div class="jenis">
        <h3 style="color: #555">{{$user->name}}</h3>
        <a>
            <div class="card">
            <div class="card-body">
                <p><i class="fas fa-user" style="margin-right: 16px"></i>Student ID
                    <br>
                    {{$user->student_id}}
                </p>
                <p><i class="fas fa-user" style="margin-right: 16px"></i>University
                    <br>
                    {{$user->university}}
                </p>
                <p><i class="fas fa-envelope" style="margin-right: 16px"></i>E-Mail
                    <br>
                    {{$user->email}}
                </p>
                <p><i class="far fa-calendar-alt" style="margin-right: 16px"></i>Date of Birth
                    <br>
                    {{$user->dob}}
                </p>
                <p>Current Plan
                    <br>
                    Free
                </p>
                <a href="profile/edit" class="btn btn-primary">Edit Profile</a>
            </div>
            </div>
        </a>

</div>
@stop


