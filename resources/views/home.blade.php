@extends('adminlte::page')

@section('title', 'Cloudent')

{{-- @section('content_header')
    <h1>
        Start Your Productivity
        <strong>Now!</strong>
    </h1>

    <ol class="breadcrumb">
        <li class="active"><a href="#"><i class="fas fa-folder-open"></i> Library</a></li>
    </ol>
@stop --}}

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Start Your Productivity
            <strong>Now!</strong></h3>
    </div>
    <div class="box-body" style="text-align: center">
        {{-- <a href="#" class="btn btn-primary">Upload Files</a> --}}
        {{-- <img  alt=""> --}}
        <form action="javascript:void(0);" method="post" enctype="multipart/form-data" name="form_upload" id="form_upload">
            @csrf
            <input type="image" src="/images/dragndrop.png"/>
            <input type="file" name="file_upload" id="my_file" style="display: none;" />
        </form>
        <br>
        <br>
        <h3 style="text-align: left">Public Data</h3>
        <hr>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped" id="public_upload" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Files</th>
                        <th>Uploaded At</th>
                        <th>Like</th>
                        <th>Dislike</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <br>
        <br>
        <h3 style="text-align: left">Your Data</h3>
        <hr>
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped" id="library_upload" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Files</th>
                        <th>Uploaded At</th>
                        <th>Like</th>
                        <th>Dislike</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop

@section('adminlte_js')
@section('js')
<script>
    function btnlike(id){
        $.ajax({
                url : "home/" + id + "/like",
                type : "PUT",
                data : {
                    'method' : 'PUT',
                    "_token": "{{ csrf_token() }}"

                    },
                    success : function(data){
                    location.reload();
                    },
                    error : function(){
                        alert("Unknown Error");
                    }
            });
    }

    function btndislike(id){
        $.ajax({
                url : "home/" + id + "/dislike",
                type : "PUT",
                data : {
                    'method' : 'PUT',
                    "_token": "{{ csrf_token() }}"

                    },
                    success : function(data){
                    location.reload();
                    },
                    error : function(){
                        alert("Unknown Error");
                    }
            });
    }

    function deleteData(id){
        if(confirm("Are you sure?")){
            $.ajax({
                url : "home/" + id,
                type : "DELETE",
                data : {
                    'method' : 'DELETE',
                    "_token": "{{ csrf_token() }}"

                    },
                    success : function(data){
                    location.reload();
                    },
                    error : function(){
                        alert("Cannot Delete Data");
                    }
            });
        }
    }

    function updateVisib(id){
        $.ajax({
                url : "home/" + id,
                type : "PUT",
                data : {
                    'method' : 'PUT',
                    "_token": "{{ csrf_token() }}"

                    },
                    success : function(data){
                    location.reload();
                    },
                    error : function(){
                        alert("Cannot Update");
                    }
            });
    }
</script>
{{-- <script src="{{asset('DataTables/datatables.min.js')}}"></script> --}}
<script>
$(document).ready(function(){

    $("input[type='image']").click(function() {
        $("input[id='my_file']").click();
    });

    $("input[type=file]").change(function(){
        document.getElementById("form_upload").action = "{{route('home.upload')}}";
        document.getElementById("form_upload").submit();
    });

    $(function () {
        table = $('#public_upload').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{route('home_public.data')}}",
                "type": "GET",
            }
        });
        // Menyimpan data form tambah/edit beserta validasinya
    });

    $(function () {
        table = $('#library_upload').DataTable({
            "processing": true,
            "serverside": true,
            "ajax": {
                "url": "{{route('home.data',auth()->user()->id)}}",
                "type": "GET",
            }
        });
        // Menyimpan data form tambah/edit beserta validasinya
    });

    // (async () => {
    // const { value: newMember } = await Swal.fire({
    // showCancelButton: true,
    // })

    // if(newMember)
    // {
        // Swal.fire(
        // 'New User ?',
        // 'Follow this simple tour to get started',
        // 'info'
        // )

        Swal.fire({
            type: 'info',
            title: 'New User ?',
            text: 'Follow this simple tour to get started',
            showCancelButton: true,
            cancelButtonText: "Skip",
        })
//     }
//     })()
});
</script>
@stop
