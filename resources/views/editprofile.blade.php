@extends('adminlte::page')

@section('title', 'Cloudent')


@section('content')
<div class="box">
    <div class="box-header with-border">
        <h1 class="box-title">Edit Your Profile</h1>
    </div>
    <br>
    <div class="jenis">
        <h3 style="color: #555">{{$user->name}}</h3>
        <a>
            <div class="card">
            <div class="card-body">
            <form action="{{route('user.update')}}" method="post">
                @csrf
                @method('PUT')
                {{-- <p><i class="fas fa-user" style="margin-right: 16px"></i>Student ID
                    <br>
                    {{$user->student_id}}
                </p>
                <p><i class="fas fa-user" style="margin-right: 16px"></i>University
                    <br>
                    {{$user->university}}
                </p> --}}
                <p><i class="fas fa-envelope" style="margin-right: 16px"></i>E-Mail
                    <br>
                    Current : {{$user->email}}
                    <br>
                    <input type="text" name="email" class="form-control">
                </p>
                <p><i class="far fa-calendar-alt" style="margin-right: 16px"></i>Date of Birth
                    <br>
                    Current : {{$user->dob}}
                    <br>
                    <input type="date" name="dob" class="form-control" placeholder="Date of Birth" autofocus>
                </p>
                <p><i class="fa fa-key" style="margin-right: 16px"></i>Old Password
                    <br>
                    <input type="password" name="old_password" class="form-control" >
                </p>
                <p><i class="fa fa-key" style="margin-right: 16px"></i>New Password
                    <br>
                    <input type="password" name="password" class="form-control" >
                </p>
                <p><i class="fa fa-key" style="margin-right: 16px"></i>Confirm Password
                    <br>
                    <input type="password" name="password_confirmation" class="form-control">
                </p>
                <input type="submit" class="btn btn-success" value="Save Changes">
            </form>
            </div>
            </div>
        </a>

</div>
@stop


