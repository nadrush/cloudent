@extends('adminlte::page')

@section('title', 'Cloudent')


@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">My Subscription Plan</h3>
    </div>
    <br><br>
    <div class="jenis">
        Silver
        <a>
            <div class="card">
            <div class="card-body">
                - 500 GB Storage<br>- Speed
                <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Average speed</div>
                </div> 
            </div>
            </div>
        </a><br>
        Gold
        <a>
            <div class="card">
            <div class="card-body">
                - 2TB Storage<br>- Speed
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">High speed</div>
                </div> 
            </div>
            </div>
        </a><br>
        <br>
        Platinum
        <a>
            <div class="card">
            <div class="card-body">
                - 10TB Storage<br>- Speed
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">Ultra speed</div>
                </div>
            </div>
            </div>
        </a><br>
        <br>
    </div>
</div>
@stop


